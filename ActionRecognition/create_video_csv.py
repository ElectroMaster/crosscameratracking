import pandas as pd
import os
import numpy as np
from random import shuffle


dataset_path = "/media/exia/data/dataset/Kinetics-custom"
dirs = os.listdir(dataset_path)
for dir in dirs:
    if not os.path.isdir(os.path.join(dataset_path, dir)):
        dirs.remove(dir)

# obtain all video classes
classes = dirs
files_length_list = []
for i, class_name in enumerate(classes):
    video_folder = os.path.join(dataset_path, class_name)
    files = os.listdir(video_folder)
    files_length_list.append(len(files))

# we only use a certain number of videos, and use 10% as validation dataset
min_length = min(files_length_list)
num_val = int(min_length*0.1)
num_train = min_length - num_val
print("For each class, we use {} video files for train, and use {} video files for validation.".format(
    num_train, num_val
))

org_video_train_list = []
label_train_list = []
org_video_val_list = []
label_val_list = []

for id, class_name in enumerate(classes):
    video_folder = os.path.join(dataset_path, class_name)
    files = os.listdir(video_folder)
    for i, file in enumerate(files):
        files[i] = os.path.join(video_folder, file)

    files_train = files[:num_train]
    files_val = files[num_train: num_train+num_val]
    labels_train = np.ones(num_train).astype(int)*id
    labels_val = np.ones(num_val).astype(int)*id
    org_video_train_list.extend(files_train)
    org_video_val_list.extend(files_val)
    label_train_list.extend(list(labels_train))
    label_val_list.extend(list(labels_val))

len_org_video_train_list = len(org_video_train_list)
len_org_video_val_list = len(org_video_val_list)
len_label_train_list = len(label_train_list)
len_label_val_list = len(label_val_list)

assert len_org_video_train_list == len_label_train_list
assert len_org_video_val_list == len_label_val_list

combined_train_list = list(zip(org_video_train_list, label_train_list))
shuffle(combined_train_list)
org_video_train_list, label_train_list = zip(*combined_train_list)

combined_val_list = list(zip(org_video_val_list, label_val_list))
shuffle(combined_val_list)
org_video_val_list, label_val_list = zip(*combined_val_list)

# write list into csv files
print("Writing into csv files ... ")
print("We have {} video files in train dataset.".format(len_org_video_train_list))
print("We have {} video files in validation dataset.".format(len_org_video_val_list))
csv_dict_train = {}
csv_dict_train['org_video'] = org_video_train_list
csv_dict_train['label'] = label_train_list

dataframe_train = pd.DataFrame(csv_dict_train)
dataframe_train.to_csv("./data/list/kinetics_custom_train.csv", index=False)

csv_dict_val = {}
csv_dict_val['org_video'] = org_video_val_list
csv_dict_val['label'] = label_val_list

dataframe_val = pd.DataFrame(csv_dict_val)
dataframe_val.to_csv("./data/list/kinetics_custom_val.csv", index=False)

f = open("./data/list/kinetics_classes.txt", "w")
for id, class_name in enumerate(classes):
    f.write(class_name+" "+str(id) + "\n")
f.close()
print("done")