import lmdb
import pandas
import sys
from caffe2.proto import caffe2_pb2
from caffe2.python import workspace
import logging

logging.basicConfig()
log = logging.getLogger("create_video_db")
log.setLevel(logging.INFO)

def create_video_db(list_file, output_file):
    lines = pandas.read_csv(list_file)
    # checking necessary fields of the provided csv file
    assert 'org_video' in lines, \
        "The input list does not have org_video column"
    assert 'label' in lines, \
        "The input list does not have label column"

    LMDB_MAP_SIZE = 1<<40
    env = lmdb.open(output_file, map_size=LMDB_MAP_SIZE)

    index = 0
    print("We have {} files in list file {}".format(lines.shape[0], list_file))
    with env.begin(write=True) as txn:
        for i, row in lines.iterrows():
            file_name = row["org_video"]
            label = row["label"]
            print("Processing video: {}".format(file_name))

            # with open(file_name, mode='rb') as file:
            #     video_data = file.read()
            video_data = file_name

            tensor_protos = caffe2_pb2.TensorProtos()
            video_tensor = tensor_protos.protos.add()
            video_tensor.data_type = 4 # string data
            video_tensor.string_data.append(video_data)

            label_tensor = tensor_protos.protos.add()
            label_tensor.data_type = 2 # int32
            label_tensor.int32_data.append(label)

            txn.put(
                "{}".format(index).encode("ascii"),
                tensor_protos.SerializeToString()
            )

            index = index + 1
            if index % 1000 == 0:
                print("processed {} videos".format(index))

# python data/create_video_db.py \
# --list_file=data/list/ucf10/ucf101_train_01.csv \
# --output_file=/data/users/trandu/datasets/ucf101_train01

if __name__ == "__main__":
    # list_file = "data/list/kinetics_custom_train.csv"
    # output_file = "/media/exia/data/dataset/Kinetics_train_lmdb"
    list_file = "data/list/kinetics_custom_val.csv"
    output_file = "/media/exia/data/dataset/Kinetics_val_lmdb"
    create_video_db(list_file, output_file)

