## Update:

- **20190416**, try to use sync TCP and try to encode image to accelerate transmission
- **20190417**, apply vibe algorithm on tcp client.
- **20190418**, add configuration files for client and server.
- **20190419**, test feasibility of python usage in c++
- **20190420**, complete database design
- **20190424**, faster rcnn too old and Lack of maintenance, decide to abandon faster rcnn and begin test yolo v3.
- **20190425**, custom voc dataset for person detection.
- **20190426**, apply original yolo v3 model on one image by using yolo c++ API

- **20190426**, train my own model for person detection -iteration 3500
- **20190429**, add login page for web interface and update database.

    <img src="./documents/figs/chart-3500.png" alt="screenshot" style="display:block; max-width:500px;">

    <table>

    <tr>
    <th>orginal yolo model</th>
    <th>custom yolo model for person detection</th>
    </tr>

    <tr>
    <td><img src="./documents/figs/predictions.jpg" alt="screenshot" style="display:block; max-width:200px;"></td>
    <td> <img src="./documents/figs/predictions-person.jpg" alt="screenshot" style="display:block; max-width:200px;"> </td>
    </tr>

    </table>

## Screenshots
**database design**
<img src="./documents/figs/database-architect.png" alt="screenshot" style="display:block; max-width:700px;">


**login page**

<img src="./documents/figs/web-login-page.png" alt="screenshot" style="display:block; max-width:700px;">

## TODO:

-   web design.

## Tasks:

**Note**: because markdown on gitlab cannot show color in the table. You can acces ![readme.pdf](./readme.pdf) for pdf version.

- **Green**: `#28a745` done.
- **Orange**: `#fc9403` not finished.
- **Red**: `#dc3545` not start yet.
- **Dark**: `#4f4f4f` not work, dead path

<table>
<tr>
<th>#</th>
<th>task</th>
<th>status</th>
<th>description</th>
</tr>

<tr>
<td>1</td>
<td>update client server</td>
<td style="background-color:#28a745;"></td>
<td>TCP proto, support multi client, binary transfer</td>
</tr>

<tr>
<td>2</td>
<td>pedestrain detection</td>
<td style="background-color:#fc9403;"></td>
<td>faster rcnn, filter images. record continuous frames.  database save</td>
</tr>

<tr>
<td>3</td>
<td>action classificationn</td>
<td style="background-color:#dc3545;"></td>
<td>use saved image for classification</td>
</tr>

<tr>
<td>4</td>
<td>web interface</td>
<td style="background-color:#fc9403;"></td>
<td>java spring, database, js, html</td>
</tr>

<tr>
<td>5</td>
<td>create database</td>
<td style="background-color: #28a745;"></td>
<td>for convention, i will use mysql. I hope usage of MySQL is similar with usage of postgreSQL</td>
</tr>

</table>

## Subtasks:

<table>

<tr>
<th>Task 1</th>
<th>sub-tasks</th>
<th>status</th>
<th>description</th>
</tr>

<tr>
<td>1</td>
<td>support tcp proto</td>
<td style="background-color:#28a745;"></td>
<td>use synchronous TCP client and server</td>
</tr>

<tr>
<td>2</td>
<td>use json message head</td>
<td style="background-color:#28a745;"></td>
<td>use nlohmann/json to send message head, include directory, filename, image data size.</td>
</tr>

<tr>
<td>3</td>
<td>binary transmission</td>
<td style="background-color:#28a745;"></td>
<td>thanks json message head, client can transfer non-fixed length image data. (unsigned char array)</td>
</tr>

<tr>
<td>4</td>
<td>support multi client</td>
<td style="background-color:#28a745;"></td>
<td>use multi thread to receive data. </td>
</tr>

<tr>
<td>5</td>
<td>apply algorithm vibe</td>
<td style="background-color:#28a745;"></td>
<td>apply ViBe </td>
</tr>

<tr>
<td>6</td>
<td>add configuration file</td>
<td style="background-color:#28a745;"></td>
<td>configure file for client and server, maybe INI, xml etc... update: finally use INI file </td>
</tr>

</table>



<table>

<tr>
<th>Task 2</th>
<th>sub-tasks</th>
<th>status</th>
<th>description</th>
</tr>

<tr>
<td>1</td>
<td>Test feasibility to see if c++ can use python code</td>
<td style="background-color:#28a745;"></td>
<td>our server use c++, but deep learning model use pycaffe. We need to test if it will work if we use python in c++</td>
</tr>

<tr>
<td>2</td>
<td>apply faster rcnn on one image</td>
<td style="background-color:#4f4f4f;"></td>
<td>test feasibility of faster rcnn.</td>
</tr>

<tr>
<td>3</td>
<td>test yolo v3</td>
<td style="background-color:#28a745;"></td>
<td>test feasibility of yolo v3.</td>
</tr>


<tr>
<td>4</td>
<td>custom voc dataset </td>
<td style="background-color:#28a745;"></td>
<td>custom my own dataset for person detection</td>
</tr>

<tr>
<td>5</td>
<td>train a model</td>
<td style="background-color:#28a745;"></td>
<td>train yolov3 for person detecction</td>
</tr>

<tr>
<td>6</td>
<td>apply original yolo model on test image with C++</td>
<td style="background-color:#28a745;"></td>
<td>test successfully on one image by using yolo c++ api</td>
</tr>

<tr>
<td>7</td>
<td>apply custom yolo model on test image with C++</td>
<td style="background-color:#28a745;"></td>
<td>test custom yolo models.</td>
</tr>


<tr>
<td>8</td>
<td>combine yolo-test with TCP client</td>
<td style="background-color:#28a745;"></td>
<td>Now camera can detect person in motion</td>

<tr>
<td>9</td>
<td>add demo video</td>
<td style="background-color:#28a745;"></td>
<td>add demo video</td>

</tr>


<tr>
<td>10</td>
<td>database stock</td>
<td style="background-color:#fc9403;"></td>
<td>stock image paths in database, maybe use postgreSQL or MySQL. update: i will use mysql</td>
</tr>

</table>





<table>

<tr>
<th>Task 5</th>
<th>sub-tasks</th>
<th>status</th>
<th>description</th>
</tr>

<tr>
<td>1</td>
<td>Database design</td>
<td style="background-color: #28a745;"></td>
<td>desing a database architecture</td>
</tr>

</table>



<table>

<tr>
<th>Task 4</th>
<th>sub-tasks</th>
<th>status</th>
<th>description</th>
</tr>

<tr>
<td>1</td>
<td>web design</td>
<td style="background-color: #fc9403;"></td>
<td>design web interface</td>
</tr>

<tr>
<td>2</td>
<td>add login page</td>
<td style="background-color: #28a745;"></td>
<td>add login page</td>
</tr>

<tr>
<td>3</td>
<td>database initialisatioin</td>
<td style="background-color: #28a745;"></td>
<td>reset ssequences of tables, add default values, and remove old connections</td>
</tr>

</table>
