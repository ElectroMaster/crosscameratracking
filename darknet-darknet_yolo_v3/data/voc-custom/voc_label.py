import xml.etree.ElementTree as ET
import pickle
import os
from os import listdir, getcwd
import sys
from os.path import join

sets = ["train", "test"]

voc_classes = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
classes = ['background', 'person']

def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def convert_annotation(image_id):
    in_file = open("Annotations/{}.xml".format(image_id))
    out_file = open("labels/{}.txt".format(image_id), "w")
    tree = ET.parse(in_file)
    root = tree.getroot()
    size = root.find("size")
    w = int(size.find("width").text)
    h = int(size.find("height").text)
    for obj in root.iter("object"):
        difficult = obj.find("difficult").text
        cls = obj.find("name").text
        if cls not in voc_classes or int(difficult) == 1:
            continue
        voc_cls_id = voc_classes.index(cls)
        person_id = voc_classes.index("person")
        if voc_cls_id == person_id:
            cls_id = 1
        else:
            cls_id = 0
        xmlbox = obj.find("bndbox")
        b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))
        bb = convert((w,h), b)
        out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')

if __name__ == "__main__":
    wd = getcwd()

    for image_set in sets:
        image_ids = open("ImageSets/Main/{}.txt".format(image_set)).read().strip().split()
        len_images = len(image_ids)
        print("There are {} images in total for {} set.".format(len_images, image_set))
        print("Begin processing ... ")
        list_file = open("{}.txt".format(image_set), 'w')
        for num, image_id in enumerate(image_ids):
            list_file.write("{}/JPEGImages/{}.jpg\n".format(wd, image_id))
            convert_annotation(image_id)
            sys.stdout.write("\r[{:.0f}%]  No. {}, image id: {}.jpg ".format(float(num)/len_images*100, num+1, image_id))
            sys.stdout.flush()
        list_file.close()
        print("\n")
