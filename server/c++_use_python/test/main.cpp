#include <Python.h>
#include <iostream>
using namespace std;
int main(){

    Py_Initialize();//使用python之前，要调用Py_Initialize();这个函数进行初始化
    if (!Py_IsInitialized())
    {
        printf("初始化失败！");
        return 0;
    }

    PyRun_SimpleString("import sys");
//    PyRun_SimpleString("import caffe");
    PyRun_SimpleString("sys.path.append('./')");//这一步很重要，修改Python路径
//    PyRun_SimpleString("print(caffe.__version__)");

    PyObject * pModule = NULL;//声明变量
    PyObject * pFunc = NULL;// 声明变量

    pModule = PyImport_ImportModule("mytest");//这里是要调用的文件名hello.py
    if (pModule == NULL)
    {
        cout << "没找到" << endl;
    }

    pFunc = PyObject_GetAttrString(pModule, "showImage");//这里是要调用的函数名
    PyObject* args = Py_BuildValue("(s)", "panda.jpg");//给python函数参数赋值

    PyObject* pRet = PyObject_CallObject(pFunc, args);//调用函数

    bool res = false;
    PyArg_Parse(pRet, "p", &res);//转换返回类型

    cout << "res:" << res << endl;//输出结果

    Py_Finalize();//调用Py_Finalize，这个根Py_Initialize相对应的。

    return 0;

}
