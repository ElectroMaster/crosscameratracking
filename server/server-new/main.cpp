#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "json.hpp"
#include <boost/filesystem.hpp>
#include "mysql/mysql.h"
#include "ini.h"

#define BUFFER_SIZE 1024
using namespace std;
using namespace cv;
using json = nlohmann::json;

typedef struct
{
    const char* server_ip;
    int server_port;
    bool show_frames;
} configuration;


struct SQL_Query_Exception : public exception {
    const char* what() const throw(){
        return "SQL Query error";
    }
};

static int config_handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("server", "server_ip")){
        pconfig->server_ip = strdup(value);
    } else if(MATCH("server", "server_port")){
        pconfig->server_port = atoi(value);
    } else if(MATCH("server", "show_frames")){
        if((strcmp(strdup(value), "true")==0)||(atoi(value) == 1)){
            pconfig->show_frames = true;
        } else {
            pconfig->show_frames = false;
        }
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}








void handle_connection(int conn, char* ipaddr, configuration config){
    MYSQL mysql;
    mysql_init(&mysql);

    mysql_real_connect(&mysql, "127.0.0.1", "exia", "exia", "venus", 3306, NULL, 0);

    pid_t pid = getpid();
    char show_frames_win_name[20];
    sprintf(show_frames_win_name, "PID: %d", pid);

    bool has_refered_to_cameras_flag = false;

    while(true){
        try{
            char msg_recv[BUFFER_SIZE];
            bzero(msg_recv, BUFFER_SIZE);
            int len;
            int ch_count = 0;
            string msg_str = "";
            while(len = recv(conn, msg_recv, BUFFER_SIZE, 0)){

                ch_count += len;
                cout<<"ch_count:"<<ch_count<<"--------len: "<<len<<endl;
                msg_str += msg_recv;

                if(msg_str.empty()){
                    ch_count = 0;
                    continue;
                }
                if(ch_count>=BUFFER_SIZE){
                    break;

                }
            }
            cout<<msg_str<<endl;
            json msg_json = json::parse(msg_str); // if message not right, this will raise an exception.
            // parse json message
            int camera_id = msg_json.at("camera_id");
            string camera_name = msg_json.at("camera_name");
            int image_data_len = msg_json.at("image_data_length");
            cout<<"[ OK ] Server received message head successfully"<<endl;
            cout<<"[INFO] Filename: "<<msg_json.at("filename")<<". Image data length: "<<image_data_len<<"."<<endl;



            // confirm info with table cameras

            char sql_array[100];
            if(!has_refered_to_cameras_flag){
                sprintf(sql_array, "SELECT * FROM cameras WHERE camera_name= '%s'", camera_name.c_str());
                string sql_str = sql_array;
                bzero(sql_array, 100);
                cout<<sql_str<<endl;
                if(int ret = mysql_query(&mysql, sql_str.c_str())){
                    throw SQL_Query_Exception();
                }
                MYSQL_RES *result = NULL;
                result = mysql_store_result( &mysql );
                int row_count = mysql_num_rows( result );
                cout<<"row_count: "<<row_count<<endl;

                if(row_count == 0){
                    // if our camera not exist in table 'cameras' we need to add it
                    cout<<"[INFO] Camera '"<<camera_name.c_str()<<"' not exist in table cameras. ";
                    cout<<"Trying add it into table ..."<<endl;
                    sprintf(sql_array, "INSERT INTO cameras (camera_id, camera_name, ip) VALUES \
    ('%d', '%s', '%s')", camera_id, camera_name.c_str(), ipaddr);
                    sql_str = sql_array;
                    bzero(sql_array, 100);
                    cout<<sql_str.c_str()<<endl;
                    if(int ret = mysql_query(&mysql, sql_str.c_str())){
                        throw SQL_Query_Exception();
                    }
                } else if(row_count>1){
                    // if more than two camera names '724', we cannot handle it. error!
                    cout<<"[ERROR] More than  two camera names '"<<camera_name.c_str()<<"'"<<endl;
                    throw SQL_Query_Exception();
                } else if (row_count ==1 ){
                    // if already exist, make sure camera info exactly same
                    cout<<"[INFO] Found camera '"<<camera_name.c_str()<<"' in table cameras.";
                    cout<<"Update camera infos"<<endl;
                    sprintf(sql_array, "UPDATE cameras SET camera_id='%d', ip = '%s' WHERE camera_name = '%s'",
                            camera_id, ipaddr, camera_name.c_str());
                    sql_str = sql_array;
                    bzero(sql_array, 100);
                    cout<<sql_str.c_str()<<endl;
                    if(int ret = mysql_query(&mysql, sql_str.c_str())){
                        throw SQL_Query_Exception();
                    }
                }
                has_refered_to_cameras_flag = true;

            }



            char msg_valide[BUFFER_SIZE] = "OK";
            len = send(conn, msg_valide, BUFFER_SIZE, 0);

            vector<uchar> vec;
            int pos = 0;
            while(pos<image_data_len){
                uchar temps_data[BUFFER_SIZE];
                memset(temps_data, 0, sizeof(temps_data));
                int len = recv(conn, temps_data, BUFFER_SIZE, 0);
                if(len<=0){
                    cout<<"len<0"<<endl;
                    shutdown(conn, SHUT_RDWR);
                    break;
                }
                for(int i=pos; i<pos+len;i++){
                    if (i>=(image_data_len))
                        break;
                    vec.push_back(temps_data[i-pos]);
                }
                pos = pos+len;
            }
            cout<<"[ OK ] Server received image data successfully."<<endl;
            len = send(conn, msg_valide, BUFFER_SIZE, 0);

            Mat image = imdecode(vec, CV_LOAD_IMAGE_COLOR);
            string images_directory = msg_json.at("images_directory");
            string file_name = msg_json.at("filename");
            boost::filesystem::path parent_dir(images_directory);
            boost::filesystem::path sub_dir(ipaddr);
            boost::filesystem::path filename(file_name);
            boost::filesystem::path dir = parent_dir/sub_dir;
            boost::filesystem::path fullpath = dir/filename;
            cout<<"[INFO] Writing image into file ";
            cout<<fullpath.c_str()<<" ..."<<endl;

            string exe_parent_path_str = getcwd(NULL,0);
            boost::filesystem::path exe_parent_path(exe_parent_path_str);
            boost::filesystem::path img_abs_path = exe_parent_path_str/fullpath;

            cout<<img_abs_path.c_str()<<endl;
//            cout<<getcwd(NULL, 0)<<endl;
            if(!boost::filesystem::is_directory(dir)){
                cout<<"Direcotry not exist, create direcotry "<<dir<<endl;
                boost::filesystem::create_directories(dir);
            }
            imwrite(fullpath.c_str(), image);
            cout<<"[ OK ] Write image into file successfully."<<endl;

            // write image file path into database
            cout<<"[INFO] Adding image file path into database. "<<endl;
            string time_cap = msg_json.at("date");
            int people_count = msg_json.at("people_count");
            string bbox_str = msg_json.at("bboxs");
            sprintf(sql_array, "INSERT INTO dynamic_frames \
                               (camera_id, frame_name, time_cap, frame_path, people_count, bboxs, action_cls_flag) VALUES \
                               ('%d', '%s', '%s', '%s', '%d', '%s', false)",
                    camera_id, file_name.c_str(),time_cap.c_str(), img_abs_path.c_str(), people_count, bbox_str.c_str());
            string sql_str = sql_array;
            bzero(sql_array, 100);
            cout<<sql_str.c_str()<<endl;
            if(int ret = mysql_query(&mysql, sql_str.c_str())){
                throw SQL_Query_Exception();
            }



            if(config.show_frames){
                imshow(show_frames_win_name, image);
                if(cv::waitKey(30) == 'q') {
                    shutdown(conn, SHUT_RDWR);
                    break;
                }
            }
        }
        catch(exception&e){
            cout<<e.what()<<endl;
            shutdown(conn, SHUT_RDWR);
            break;
        }
        cout<<"-------------"<<endl;
    }
    cout<<"close connection"<<endl;
    mysql_close( &mysql );
    close(conn);
}



int main(){


    configuration config;
    if (ini_parse("config-serv.ini", config_handler, &config) < 0) {
        printf("[ERROR] Can't load configuration file.\n");
        return -1;
    }

    int server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(config.server_ip);
    server_addr.sin_port = htons(config.server_port);
    bind(server_sock, (struct sockaddr*)&server_addr, sizeof(server_addr));




    if(listen(server_sock, 20)<0){
        perror("[ERROR] LISTEN");
        exit(-1);
    }else{
        cout<<"[INFO] Server is listening ..."<<endl;
        cout<<endl;
    }

    while(true){
        struct sockaddr_in client_addr;
        socklen_t client_addr_size = sizeof(client_addr);
        int conn = accept(server_sock, (struct sockaddr*)&client_addr, &client_addr_size);
        if (conn<0){
            perror("[ERROR] CONNECT");
            exit(-1);
        }
        char* client_ipaddr = inet_ntoa(client_addr.sin_addr);
        cout<<"[INFO] New client connected: "<<client_ipaddr<<endl;
//        char addressBuffer[INET_ADDRSTRLEN];
//        inet_ntop(AF_INET, &client_addr.sin_addr, addressBuffer, INET_ADDRSTRLEN);
//        cout<<addressBuffer<<endl;

        pid_t childid;
        if(childid=fork() == 0){
            printf("[INFO] Child process: %d created.\n\n", getpid());
            close(server_sock); // close listen in child pid
            handle_connection(conn,client_ipaddr, config);
            exit(0);

        }

    }
    close(server_sock);
    return 0;


}
