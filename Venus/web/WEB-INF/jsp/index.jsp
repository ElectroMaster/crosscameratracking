<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Venus</title>
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <link rel="icon" href="img/buaa-logo.gif" >
        
    </head>

    <body>
        <form:form action="test.html" method="POST">
            <table>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><img class="img-header" src="img/vrlab-logo-text.jpg" alt="vrlab logo" width="300"></td>
                </tr>
                <tr>
                    <td rowspan="5">
                        <img class="img-left" src="img/deep-learning.png" alt="DeepLearning" width="150">
                    </td>
                    <td colspan="2" class="header">Venus</td>
                </tr>
                <tr>
                    <td colspan="2"><p class="error">
                        <c:choose>
                            <c:when test="${not empty message}">${message}</c:when>
                            <%--<c:otherwise>&nbsp;</c:otherwise>--%>
                        </c:choose>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="text" name="login" placeholder="Username"></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="password" name="passw" placeholder="Password"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit">LOG IN</button></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
            </table>
        </form:form>
        <footer>
            @Equipe Venus - State Key Laboratory of Virtual Reality Technology and Systems - BUAA - China
        </footer>
    </body>
</html>
