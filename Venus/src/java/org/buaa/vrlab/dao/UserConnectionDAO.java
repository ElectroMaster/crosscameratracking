/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

import java.util.Collection;
import java.util.Date;
import org.buaa.vrlab.items.UserConnection;

/**
 *
 * @author exia
 */
public interface UserConnectionDAO extends DAO<UserConnection, String>{
    public Collection<UserConnection> getAll();
    
    public Collection<UserConnection> getConnectionsBefore(Date specified_time);
    
    public UserConnection findByConnectionCode(String code);
    
    public void create(UserConnection item);
    
    public void remove(UserConnection item);
}
