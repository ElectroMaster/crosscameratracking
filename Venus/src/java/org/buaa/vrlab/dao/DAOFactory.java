/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

/**
 *
 * @author exia
 */
public abstract class DAOFactory {
    public static UserDAO getUserDAO(){
        return new UserDAOImpl();
    }
    
    public static ProfilDAO getProfilDAO(){
        return new ProfilDAOImpl();
    }
    
    public static UserConnectionDAO getUserConnectionDAO(){
        return new UserConnectionDAOImpl();
    }
}
