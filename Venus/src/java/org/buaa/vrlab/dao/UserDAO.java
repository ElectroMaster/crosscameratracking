/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

import java.util.Collection;
import org.buaa.vrlab.items.Profil;
import org.buaa.vrlab.items.User;

/**
 *
 * @author exia
 */
public interface UserDAO extends DAO<User, Integer> {
    public Collection<User> getAll();
    
    public User findById(Integer id);
    public User findById(int id);
    
    public User findByUid(String uid);
    
    public User getUserByLoginPasswd(String login, String passwd);
    
    public void create(User item);
    public User create(String surname, String name, String uid, String password,
            String email, String telphone, Profil profil, boolean user_valide);
    public void create(Integer id, String surname, String name, String uid, String password,
        String email, String telphone, Profil profil, boolean user_valide);

    public void remove(User item);
    
    public void update(Integer id, User item);
    

    
}
