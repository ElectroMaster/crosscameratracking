/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

import java.util.Collection;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.buaa.vrlab.items.UserConnection;
import org.buaa.vrlab.managers.ItemManager;

/**
 *
 * @author exia
 */
public class UserConnectionDAOImpl implements UserConnectionDAO{
    
    @Override
    public Collection<UserConnection> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query query = em.createNamedQuery("UserConnection.findAll", UserConnection.class);
        Collection theList = query.getResultList();
        return theList;
    }
    
    @Override
    public Collection<UserConnection> getConnectionsBefore(Date specified_time){
        EntityManager em = ItemManager.getEntityManager();
        Query query = em.createNamedQuery("UserConnection.findByConnectionExpireBefore", UserConnection.class);
        query.setParameter("connectionExpire", specified_time);
        Collection theList = query.getResultList();
        return theList;
    }
    
    @Override
    public UserConnection findByConnectionCode(String code){
        UserConnection conn = null;
        if((code!=null) && (!code.isEmpty())){
            EntityManager em = ItemManager.getEntityManager();
            Query query = em.createNamedQuery("UserConnection.findByConnectionCode", UserConnection.class);
            query.setParameter("connectionCode", code);
            try{
                conn = (UserConnection) query.getSingleResult();
                return conn;
            } catch(Exception ex){
                return null;
            }
        } else {
            return null;
        }
    }
    
    @Override
    public void create(UserConnection item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(item);
        em.flush();
        transaction.commit();
    }

    @Override
    public void remove(UserConnection item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(item);
        em.flush();
        transaction.commit();
    }
}


