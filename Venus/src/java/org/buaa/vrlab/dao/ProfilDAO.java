/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

import java.util.Collection;
import org.buaa.vrlab.items.Profil;

/**
 *
 * @author exia
 */
public interface ProfilDAO extends DAO<Profil, Integer> {
    public Collection<Profil> getAll();
    
    public Profil findById(Integer id);
    public Profil findById(int id);
    
    public void create(Profil profil);
    public Profil create(String privilage);
    public void create(Integer id, String privilage);
    
    public void remove(Profil item);
    public void update(Integer id, Profil item);
}
