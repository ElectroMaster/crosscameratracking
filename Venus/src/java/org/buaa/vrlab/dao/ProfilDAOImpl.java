/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.buaa.vrlab.items.Profil;
import org.buaa.vrlab.managers.ItemManager;

/**
 *
 * @author exia
 */
public class ProfilDAOImpl implements ProfilDAO{
    @Override
    public Collection<Profil> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query query = em.createNamedQuery("Profil.findAll", Profil.class);
        Collection<Profil> theList = query.getResultList();
        return theList;
    }
    
    @Override
    public Profil findById(Integer id){
        Profil profil = null;
        try{
            EntityManager em = ItemManager.getEntityManager();
            Query query = em.createNamedQuery("Profil.findByProfilId", Profil.class);
            query.setParameter("profilId", id);
            profil = (Profil) query.getSingleResult();
        } catch (NoResultException ex){
            return null;
        }
        return profil;
        
    }
    
    @Override
    public Profil findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public void create(Profil profil){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(profil);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void create(Integer id, String privilage){
        Profil profil = this.findById(id);
        if(profil == null){
            profil = new Profil();
            profil.setProfilId(id);
            profil.setPrivilage(privilage);
            this.create(profil);
        }
    }
    
    @Override
    public Profil create(String privilage){
        Profil profil = new Profil();
        profil.setPrivilage(privilage);
        ProfilDAO profilDAO = DAOFactory.getProfilDAO();
        profilDAO.create(profil);
        return profil;
    }
    
    @Override
    public void remove(Profil item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(item);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void update(Integer id, Profil item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Profil profil = this.findById(id);
        profil.setPrivilage(item.getPrivilage());
        em.flush();
        transaction.commit();
    }
}
