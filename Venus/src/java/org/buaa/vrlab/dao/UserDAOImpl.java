/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.dao;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.buaa.vrlab.items.Profil;
import org.buaa.vrlab.items.User;
import org.buaa.vrlab.managers.ItemManager;
import org.buaa.vrlab.utilities.Utils;

/**
 *
 * @author exia
 */
public class UserDAOImpl implements UserDAO{
    @Override
    public Collection<User> getAll(){
        EntityManager em = ItemManager.getEntityManager();
        Query query = em.createNamedQuery("User.findAll", User.class);
        Collection theList = query.getResultList();
        return theList;
    }
    
    @Override
    public User findById(Integer id){
        User user = null;
        try{
            EntityManager em = ItemManager.getEntityManager();
            Query query = em.createNamedQuery("User.findByUserId", User.class);
            query.setParameter("userId", id);
            user = (User) query.getSingleResult();
        }catch(NoResultException ex){
            return null;
        }

        return user;
        
    }
    @Override
    public User findById(int id){
        return findById(new Integer(id));
    }
    
    @Override
    public User findByUid(String uid){
        User user = null;
        try{
            EntityManager em = ItemManager.getEntityManager();
            Query query = em.createNamedQuery("User.findByUid", User.class);
            query.setParameter("uid", uid);
            user = (User) query.getSingleResult();
        }catch(NoResultException ex){
            return null;
        }
        
        return user;
    }
    
    @Override
    public User getUserByLoginPasswd(String login, String passwd){

        User anUser = null;
        if((login != null) && (passwd != null) &&
                (!login.isEmpty()) && (!passwd.isEmpty())){
            EntityManager em = ItemManager.getEntityManager();
            Query query = em.createNativeQuery("SELECT user.*"
                + " FROM user"
                + " WHERE uid=?1"
                + " AND user_valide=TRUE", User.class);
            query.setParameter(1, login);
            try {
                Collection<User> theList = query.getResultList();
                for (User item: theList){
                    if((item.getPassword() != null) && (!item.getPassword().isEmpty())){
                        if(item.getPassword().equals(Utils.encodePasswd(passwd))){
                            anUser = item;
                            break;
                        }
                    }
                }
                return anUser;
            } catch(NoResultException ex){
                // do nothing
            }
        }
        System.out.println("null user");
        return anUser;
    }
    
    @Override
    public void create(User item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(item);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void create(Integer id, String surname, String name, String uid, String password,
            String email, String telphone, Profil profil, boolean user_valide){
        User user = this.findById(id);
        if(user == null){

            user = new User();
            user.setUserId(id);
            user.setSurname(surname);
            user.setName(name);
            user.setUid(uid);
            user.setPassword(password);
            user.setEmail(email);
            user.setTelephone(telphone);
            user.setProfilId(profil);
            user.setUserValide(user_valide);
            this.create(user);
        }
    }
    
    @Override
    public User create(String surname, String name, String uid, String password,
            String email, String telphone, Profil profil, boolean user_valide){
        User user = new User();
        user.setSurname(surname);
        user.setName(name);
        user.setUid(uid);
        user.setPassword(password);
        user.setEmail(email);
        user.setTelephone(telphone);
        user.setProfilId(profil);
        user.setUserValide(user_valide);
        UserDAO userDAO = DAOFactory.getUserDAO();
        userDAO.create(user);
        return user;
    }

    @Override
    public void remove(User item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(item);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void update(Integer id, User item){
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        User user = this.findById(id);
        user.setSurname(item.getSurname());
        user.setName(item.getName());
        user.setUid(item.getUid());
        user.setPassword(item.getPassword());
        user.setEmail(item.getEmail());
        user.setTelephone(item.getTelephone());
        user.setProfilId(item.getProfilId());
        em.flush();
        transaction.commit();
    }
}
