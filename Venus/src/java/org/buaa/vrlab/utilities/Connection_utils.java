/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.utilities;

import java.util.Calendar;

/**
 *
 * @author exia
 */
public class Connection_utils {
    private static final int MAXCODE = 32;
    /**
     * Create unique random code
     *
     * @return code
     */
    public static String createRandomCode() {
        StringBuilder code = new StringBuilder();

        // Start with character
        code.append((char) ('A' + (int) (Math.random() * 26)));

        // Fill with current timestamp
        long now = Calendar.getInstance().getTime().getTime();
        int index = 0;
        long value = now;
        while ((value > 0) || (index < MAXCODE - 5)) {
            code.append((char) ('0' + (value % 10)));
            value = value / 10;
            index++;
        }
        // Add any sequance of 4 characters
        code.append((char) ('A' + (int) (Math.random() * 26)));
        code.append((char) ('A' + (int) (Math.random() * 26)));
        code.append((char) ('A' + (int) (Math.random() * 26)));
        code.append((char) ('A' + (int) (Math.random() * 26)));

        return code.toString();
    }

}
