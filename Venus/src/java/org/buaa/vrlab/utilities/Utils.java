/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.utilities;



import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.buaa.vrlab.dao.DAOFactory;
import org.buaa.vrlab.dao.ProfilDAO;
import org.buaa.vrlab.dao.UserConnectionDAO;
import org.buaa.vrlab.dao.UserDAO;
import org.buaa.vrlab.items.Profil;
import org.buaa.vrlab.items.UserConnection;

import org.buaa.vrlab.managers.ItemManager;

/**
 *
 * @author exia
 */
@MultipartConfig
public class Utils {
    
    private static final int ADMINISTRATOR = 1;
    private static final int MAINTAINER = 2;
    private static final int GUEST = 3;
    
    private static final String UPLOAD_DIRECTORY = "tmp";
    private static final String DUMMYATTRIBUTE = "DUMA_INITED";
    private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB
    
    /**
     * Dabase Initialisation: Reset database sequences
     */
    public static void resetSequences(){
        EntityManager em = ItemManager.getEntityManager();
        HashMap<String, String> tables = new HashMap<>();
        tables.put("user", "user_id");
        tables.put("profil", "profil_id");
        tables.put("actions", "action_id");
        
        for(String aTable : tables.keySet()){
            EntityTransaction transaction = ItemManager.getTransaction();
            transaction.begin();
            // if no data in table, table id begin from 1 
            Query query = em.createNativeQuery("ALTER TABLE " + aTable+ " AUTO_INCREMENT = 1");
            query.executeUpdate();
            transaction.commit();
        } 
    }
    /**
     * Dabase Initialisation: Create default values for tables
     */
    public static void createDefault(){
        
        // do not change order of code here!!!
        ProfilDAO profilDAO = DAOFactory.getProfilDAO();
        profilDAO.create(ADMINISTRATOR, "Administrator");
        profilDAO.create(MAINTAINER, "Maintainer");
        profilDAO.create(GUEST, "Guest");



        
        UserDAO userDAO = DAOFactory.getUserDAO();
        Profil adminProfil = profilDAO.findById(ADMINISTRATOR);
        userDAO.create(1, "", "", "admin", encodePasswd("12345"), "", "", adminProfil, true);
        
        
    }
    
    
    public static void removeOldConnections() {
        Date now_t = getCurrentTime();
        UserConnectionDAO userConnectionDAO = DAOFactory.getUserConnectionDAO();
        Collection<UserConnection> conns = userConnectionDAO.getConnectionsBefore(now_t);
        if(!conns.isEmpty()){
            for(UserConnection conn : conns){
                userConnectionDAO.remove(conn);
            }
        }
    }
    
    /**
     * Get current date and converst it to a SQL timestamp
     *
     * @return
     */
    public static Date getCurrentTime() {
        Calendar aCalendar = Calendar.getInstance();
        return aCalendar.getTime();
    }
    
    public static String getCurrentTimeAsSQL(){
        Calendar aCalendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return "TO_DATE('"+ sdf.format(aCalendar.getTime()) + "', 'YYYY-MM-DD HH24:MI:SS'";
    }
    
    
public static String getRequestString(HttpServletRequest request, String name) {
        if (ServletFileUpload.isMultipartContent(request)) {
            if (request.getAttribute(DUMMYATTRIBUTE) == null) {
                // Multipart attribute not processed
                request.setAttribute(DUMMYATTRIBUTE, "1");

                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(MEMORY_THRESHOLD);
                factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
                ServletFileUpload upload = new ServletFileUpload(factory);
                upload.setFileSizeMax(MAX_FILE_SIZE);
                upload.setSizeMax(MAX_REQUEST_SIZE);
                
                    //                String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
                try {
                    FileItemIterator fIterator = upload.getItemIterator(request);
                    while (fIterator.hasNext()) {
                        FileItemStream fItem = fIterator.next();
                        if (fItem.isFormField()) {
                            String fieldname = fItem.getFieldName();
                            //String fieldvalue = fItem.getString();
                            request.setAttribute(fieldname, "");
                        } else {
                            
                        }
                    }
                } catch (FileUploadException ex) {
                    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String value = (String) request.getAttribute(name);
            if (value != null) {
                // value is defined
                return value;
            }
        } else {
            String value = request.getParameter(name);
            if (value != null) {
                // value is a parameter => return value
                return value;
            } else {
                // Value is not a parameter. An attribute ?
                value = (String) request.getAttribute(name);
                if (value != null) {
                    // value is an attribute
                    return value;
                }
            }
        }
        // Unknown, return empty string
        return "";
    }

    public static String encodePasswd(String passwd){
        byte[] encodedBytes = DigestUtils.sha256(passwd);
        String encoded = new String(encodedBytes);
        return encoded;
    }





}
