/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.utilities;

import org.buaa.vrlab.dao.DAOFactory;
import org.buaa.vrlab.dao.UserDAO;
import org.buaa.vrlab.items.User;
import org.buaa.vrlab.items.User_t;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author exia
 */
public class UserLoginValidator implements Validator{
    
    @Override
    public boolean supports(Class type){
        return User_t.class.isAssignableFrom(type);
    }
    @Override
    public void validate(Object o, Errors errors){
        User_t anUser = (User_t) o;
        String login = anUser.getLogin();
        String passwd = anUser.getPassw();
        if((login == null) || (login.equals(""))){
            ValidationUtils.rejectIfEmpty(errors, "login", "login.empty", "Login not defined");
        } else if((passwd == null) || (passwd.equals(""))){
            ValidationUtils.rejectIfEmpty(errors, "passw", "passw.emtpy","Password not defined");
        } else {
            if(this.identifyDatabase(anUser.getLogin(), anUser.getPassw()) == null){
                errors.reject("Invalid", "Login/Password incorrect");
            }
        }
    }
    
    
    private User identifyDatabase(String login, String passwd){
        UserDAO userDAO = DAOFactory.getUserDAO();
        return userDAO.getUserByLoginPasswd(login, passwd);
    }
    
    public User getUser(String login, String passwd){
        if((login == null) || (login.equals(""))){
            return null;
        } else {
            UserDAO userDAO = DAOFactory.getUserDAO();
            return userDAO.getUserByLoginPasswd(login, passwd);
        }
    }
}
