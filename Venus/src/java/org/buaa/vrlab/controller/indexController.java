/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.controller;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.buaa.vrlab.dao.DAOFactory;
import org.buaa.vrlab.dao.UserConnectionDAO;
import org.buaa.vrlab.items.User;
import org.buaa.vrlab.items.UserConnection;
import org.buaa.vrlab.items.User_t;
import org.buaa.vrlab.managers.ItemManager;
import org.buaa.vrlab.utilities.Connection_utils;
import org.buaa.vrlab.utilities.UserLoginValidator;
import org.buaa.vrlab.utilities.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class indexController {
    
    private static boolean initialised = false;
    
    /**
     * if login page is invoked from other page, we need to check if we have already
     * login
     * @param request
     * @return 
     */
    public static UserConnection check(HttpServletRequest request){
        // remove old connections
//        Utils.removeOldConnections();

        String userIdCode = Utils.getRequestString(request, "uid");
        UserConnectionDAO userConnectionDAO = DAOFactory.getUserConnectionDAO();
        UserConnection conn = userConnectionDAO.findByConnectionCode(userIdCode);
        if(conn != null){
            long DELTA = 1800000;
            Date newexpire = new Date(conn.getConnectionExpire().getTime()+DELTA);
            if(newexpire.getTime() > Utils.getCurrentTime().getTime()){

                // if not timed out, update expire time 
                EntityManager em = ItemManager.getEntityManager();
                EntityTransaction transaction = ItemManager.getTransaction();
                transaction.begin();
                conn.setConnectionExpire(Utils.getCurrentTime());
                em.flush();
                transaction.commit();
            } else{
                // if time out, remove old connections
                Utils.removeOldConnections();
            }
        } else{
            // if havenot log in 
            Utils.removeOldConnections();
        }
        return conn;
    }
    
    
    public static void checkDefault() {
        if (!initialised) {
            // set default values;
            Utils.resetSequences();
            Utils.createDefault();
            Utils.removeOldConnections();
            initialised = true;
        }
    }

    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
        checkDefault();
        return new ModelAndView("index");
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView handlePost(@ModelAttribute("User_t") User_t anUser, 
            BindingResult result, HttpServletRequest request, 
            HttpServletResponse response){
        
        ModelAndView returned;
        check(request);
        User aUser = null;
        UserLoginValidator validator = new UserLoginValidator();
        validator.validate(anUser, result);
        if(!result.hasErrors()){
            aUser = validator.getUser(anUser.getLogin(), anUser.getPassw());
        }
        
        if(aUser == null){
            returned = new ModelAndView("index");
            if(result.getErrorCount() == 0){
                returned.addObject("message", "Connection refused");
            } else {
                StringBuilder message = new StringBuilder();
                for(ObjectError msg: result.getAllErrors()){
                    message.append(msg.getDefaultMessage());
                }
                returned.addObject("message", message.toString());
            }
            
        } else{
            // ok we have logged in successfully. 
            
            UserConnection new_conn = new UserConnection();
            new_conn.setConnectionCode(Connection_utils.createRandomCode());
            new_conn.setUserId(aUser);
            new_conn.setConnectionExpire(Utils.getCurrentTime());
            UserConnectionDAO userConnectionDAO = DAOFactory.getUserConnectionDAO();
            userConnectionDAO.create(new_conn);
            
            returned = new ModelAndView("test");
        }
        
        return returned;
    }
}
