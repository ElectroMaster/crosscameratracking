/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.controller;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.buaa.vrlab.dao.DAOFactory;
import org.buaa.vrlab.dao.UserConnectionDAO;
import org.buaa.vrlab.items.User;
import org.buaa.vrlab.items.UserConnection;
import org.buaa.vrlab.items.User_t;
import org.buaa.vrlab.managers.ItemManager;
import org.buaa.vrlab.utilities.Connection_utils;
import org.buaa.vrlab.utilities.UserLoginValidator;
import org.buaa.vrlab.utilities.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class testController {
    


    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){

        return new ModelAndView("test");
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView handlePost(HttpServletRequest request,
            HttpServletResponse response){

        return new ModelAndView("test");
    
    }
}
