/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "dynamic_frames")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DynamicFrames.findAll", query = "SELECT d FROM DynamicFrames d"),
    @NamedQuery(name = "DynamicFrames.findByFrameId", query = "SELECT d FROM DynamicFrames d WHERE d.frameId = :frameId"),
    @NamedQuery(name = "DynamicFrames.findByFrameName", query = "SELECT d FROM DynamicFrames d WHERE d.frameName = :frameName"),
    @NamedQuery(name = "DynamicFrames.findByTimeCap", query = "SELECT d FROM DynamicFrames d WHERE d.timeCap = :timeCap"),
    @NamedQuery(name = "DynamicFrames.findByFramePath", query = "SELECT d FROM DynamicFrames d WHERE d.framePath = :framePath"),
    @NamedQuery(name = "DynamicFrames.findByPeopleCount", query = "SELECT d FROM DynamicFrames d WHERE d.peopleCount = :peopleCount"),
    @NamedQuery(name = "DynamicFrames.findByBboxs", query = "SELECT d FROM DynamicFrames d WHERE d.bboxs = :bboxs"),
    @NamedQuery(name = "DynamicFrames.findByActionClsFlag", query = "SELECT d FROM DynamicFrames d WHERE d.actionClsFlag = :actionClsFlag")})
public class DynamicFrames implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "frame_id")
    private Integer frameId;
    @Column(name = "frame_name")
    private String frameName;
    @Column(name = "time_cap")
    private String timeCap;
    @Column(name = "frame_path")
    private String framePath;
    @Column(name = "people_count")
    private Integer peopleCount;
    @Column(name = "bboxs")
    private String bboxs;
    @Column(name = "action_cls_flag")
    private Boolean actionClsFlag;
    @JoinColumn(name = "action_id", referencedColumnName = "action_id")
    @ManyToOne
    private Actions actionId;
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id")
    @ManyToOne
    private Cameras cameraId;

    public DynamicFrames() {
    }

    public DynamicFrames(Integer frameId) {
        this.frameId = frameId;
    }

    public Integer getFrameId() {
        return frameId;
    }

    public void setFrameId(Integer frameId) {
        this.frameId = frameId;
    }

    public String getFrameName() {
        return frameName;
    }

    public void setFrameName(String frameName) {
        this.frameName = frameName;
    }

    public String getTimeCap() {
        return timeCap;
    }

    public void setTimeCap(String timeCap) {
        this.timeCap = timeCap;
    }

    public String getFramePath() {
        return framePath;
    }

    public void setFramePath(String framePath) {
        this.framePath = framePath;
    }

    public Integer getPeopleCount() {
        return peopleCount;
    }

    public void setPeopleCount(Integer peopleCount) {
        this.peopleCount = peopleCount;
    }

    public String getBboxs() {
        return bboxs;
    }

    public void setBboxs(String bboxs) {
        this.bboxs = bboxs;
    }

    public Boolean getActionClsFlag() {
        return actionClsFlag;
    }

    public void setActionClsFlag(Boolean actionClsFlag) {
        this.actionClsFlag = actionClsFlag;
    }

    public Actions getActionId() {
        return actionId;
    }

    public void setActionId(Actions actionId) {
        this.actionId = actionId;
    }

    public Cameras getCameraId() {
        return cameraId;
    }

    public void setCameraId(Cameras cameraId) {
        this.cameraId = cameraId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (frameId != null ? frameId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DynamicFrames)) {
            return false;
        }
        DynamicFrames other = (DynamicFrames) object;
        if ((this.frameId == null && other.frameId != null) || (this.frameId != null && !this.frameId.equals(other.frameId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.DynamicFrames[ frameId=" + frameId + " ]";
    }
    
}
