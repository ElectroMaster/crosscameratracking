/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "ia_config")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IaConfig.findAll", query = "SELECT i FROM IaConfig i"),
    @NamedQuery(name = "IaConfig.findByConfigId", query = "SELECT i FROM IaConfig i WHERE i.configId = :configId"),
    @NamedQuery(name = "IaConfig.findByAnalyseInterval", query = "SELECT i FROM IaConfig i WHERE i.analyseInterval = :analyseInterval"),
    @NamedQuery(name = "IaConfig.findByPicCleanFlag", query = "SELECT i FROM IaConfig i WHERE i.picCleanFlag = :picCleanFlag"),
    @NamedQuery(name = "IaConfig.findByHideNotrunningFlag", query = "SELECT i FROM IaConfig i WHERE i.hideNotrunningFlag = :hideNotrunningFlag")})
public class IaConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "config_id")
    private Integer configId;
    @Column(name = "analyse_interval")
    private String analyseInterval;
    @Column(name = "pic_clean_flag")
    private String picCleanFlag;
    @Column(name = "hide_notrunning_flag")
    private String hideNotrunningFlag;

    public IaConfig() {
    }

    public IaConfig(Integer configId) {
        this.configId = configId;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public String getAnalyseInterval() {
        return analyseInterval;
    }

    public void setAnalyseInterval(String analyseInterval) {
        this.analyseInterval = analyseInterval;
    }

    public String getPicCleanFlag() {
        return picCleanFlag;
    }

    public void setPicCleanFlag(String picCleanFlag) {
        this.picCleanFlag = picCleanFlag;
    }

    public String getHideNotrunningFlag() {
        return hideNotrunningFlag;
    }

    public void setHideNotrunningFlag(String hideNotrunningFlag) {
        this.hideNotrunningFlag = hideNotrunningFlag;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (configId != null ? configId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IaConfig)) {
            return false;
        }
        IaConfig other = (IaConfig) object;
        if ((this.configId == null && other.configId != null) || (this.configId != null && !this.configId.equals(other.configId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.IaConfig[ configId=" + configId + " ]";
    }
    
}
