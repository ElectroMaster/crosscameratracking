/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "user_connection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserConnection.findAll", query = "SELECT u FROM UserConnection u"),
    @NamedQuery(name = "UserConnection.findByConnectionCode", query = "SELECT u FROM UserConnection u WHERE u.connectionCode = :connectionCode"),
    @NamedQuery(name = "UserConnection.findByConnectionExpire", query = "SELECT u FROM UserConnection u WHERE u.connectionExpire = :connectionExpire"),
    @NamedQuery(name = "UserConnection.findByConnectionExpireBefore", query = "SELECT u FROM UserConnection u WHERE u.connectionExpire <= :connectionExpire")})
public class UserConnection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "connection_code")
    private String connectionCode;
    @Basic(optional = false)
    @Column(name = "connection_expire")
    @Temporal(TemporalType.TIMESTAMP)
    private Date connectionExpire;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private User userId;

    public UserConnection() {
    }

    public UserConnection(String connectionCode) {
        this.connectionCode = connectionCode;
    }

    public UserConnection(String connectionCode, Date connectionExpire) {
        this.connectionCode = connectionCode;
        this.connectionExpire = connectionExpire;
    }

    public String getConnectionCode() {
        return connectionCode;
    }

    public void setConnectionCode(String connectionCode) {
        this.connectionCode = connectionCode;
    }

    public Date getConnectionExpire() {
        return connectionExpire;
    }

    public void setConnectionExpire(Date connectionExpire) {
        this.connectionExpire = connectionExpire;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (connectionCode != null ? connectionCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserConnection)) {
            return false;
        }
        UserConnection other = (UserConnection) object;
        if ((this.connectionCode == null && other.connectionCode != null) || (this.connectionCode != null && !this.connectionCode.equals(other.connectionCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.UserConnection[ connectionCode=" + connectionCode + " ]";
    }
    
}
