/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "ia_alarm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IaAlarm.findAll", query = "SELECT i FROM IaAlarm i"),
    @NamedQuery(name = "IaAlarm.findByAlarmId", query = "SELECT i FROM IaAlarm i WHERE i.alarmId = :alarmId"),
    @NamedQuery(name = "IaAlarm.findByAlarmType", query = "SELECT i FROM IaAlarm i WHERE i.alarmType = :alarmType"),
    @NamedQuery(name = "IaAlarm.findByServerId", query = "SELECT i FROM IaAlarm i WHERE i.serverId = :serverId"),
    @NamedQuery(name = "IaAlarm.findByAlarmState", query = "SELECT i FROM IaAlarm i WHERE i.alarmState = :alarmState"),
    @NamedQuery(name = "IaAlarm.findByPhotoAddress", query = "SELECT i FROM IaAlarm i WHERE i.photoAddress = :photoAddress"),
    @NamedQuery(name = "IaAlarm.findByNvrTime", query = "SELECT i FROM IaAlarm i WHERE i.nvrTime = :nvrTime"),
    @NamedQuery(name = "IaAlarm.findByAddTime", query = "SELECT i FROM IaAlarm i WHERE i.addTime = :addTime"),
    @NamedQuery(name = "IaAlarm.findByAlarmVpt", query = "SELECT i FROM IaAlarm i WHERE i.alarmVpt = :alarmVpt"),
    @NamedQuery(name = "IaAlarm.findByRemark", query = "SELECT i FROM IaAlarm i WHERE i.remark = :remark")})
public class IaAlarm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "alarm_id")
    private Integer alarmId;
    @Column(name = "alarm_type")
    private String alarmType;
    @Column(name = "server_id")
    private String serverId;
    @Column(name = "alarm_state")
    private Integer alarmState;
    @Column(name = "photo_address")
    private String photoAddress;
    @Column(name = "nvr_time")
    private String nvrTime;
    @Column(name = "add_time")
    private String addTime;
    @Column(name = "alarm_vpt")
    private String alarmVpt;
    @Column(name = "remark")
    private String remark;
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id")
    @ManyToOne
    private Cameras cameraId;

    public IaAlarm() {
    }

    public IaAlarm(Integer alarmId) {
        this.alarmId = alarmId;
    }

    public Integer getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(Integer alarmId) {
        this.alarmId = alarmId;
    }

    public String getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public Integer getAlarmState() {
        return alarmState;
    }

    public void setAlarmState(Integer alarmState) {
        this.alarmState = alarmState;
    }

    public String getPhotoAddress() {
        return photoAddress;
    }

    public void setPhotoAddress(String photoAddress) {
        this.photoAddress = photoAddress;
    }

    public String getNvrTime() {
        return nvrTime;
    }

    public void setNvrTime(String nvrTime) {
        this.nvrTime = nvrTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getAlarmVpt() {
        return alarmVpt;
    }

    public void setAlarmVpt(String alarmVpt) {
        this.alarmVpt = alarmVpt;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Cameras getCameraId() {
        return cameraId;
    }

    public void setCameraId(Cameras cameraId) {
        this.cameraId = cameraId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alarmId != null ? alarmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IaAlarm)) {
            return false;
        }
        IaAlarm other = (IaAlarm) object;
        if ((this.alarmId == null && other.alarmId != null) || (this.alarmId != null && !this.alarmId.equals(other.alarmId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.IaAlarm[ alarmId=" + alarmId + " ]";
    }
    
}
