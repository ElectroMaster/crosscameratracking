/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

/**
 *
 * @author exia
 */
public class User_t {
    private String login;
    private String passw;
    
    public User_t(String login, String passw){
        this.login = login;
        this.passw = passw;
    }
    public User_t(){
        
    }
    
    public String getLogin(){
        return login;
    }
    
    public void setLogin(String login){
        this.login = login;
    }
    
    public String getPassw(){
        return passw;
    }
    
    public void setPassw(String passw){
        this.passw = passw;
    }

}
