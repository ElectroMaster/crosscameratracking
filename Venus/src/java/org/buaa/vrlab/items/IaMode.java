/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "ia_mode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IaMode.findAll", query = "SELECT i FROM IaMode i"),
    @NamedQuery(name = "IaMode.findByModeId", query = "SELECT i FROM IaMode i WHERE i.modeId = :modeId"),
    @NamedQuery(name = "IaMode.findByModeName", query = "SELECT i FROM IaMode i WHERE i.modeName = :modeName"),
    @NamedQuery(name = "IaMode.findByModeType", query = "SELECT i FROM IaMode i WHERE i.modeType = :modeType"),
    @NamedQuery(name = "IaMode.findByModeWeek", query = "SELECT i FROM IaMode i WHERE i.modeWeek = :modeWeek"),
    @NamedQuery(name = "IaMode.findByModeTime", query = "SELECT i FROM IaMode i WHERE i.modeTime = :modeTime"),
    @NamedQuery(name = "IaMode.findByModeCameras", query = "SELECT i FROM IaMode i WHERE i.modeCameras = :modeCameras"),
    @NamedQuery(name = "IaMode.findByModePara", query = "SELECT i FROM IaMode i WHERE i.modePara = :modePara"),
    @NamedQuery(name = "IaMode.findByModeFlag", query = "SELECT i FROM IaMode i WHERE i.modeFlag = :modeFlag")})
public class IaMode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "mode_id")
    private Integer modeId;
    @Column(name = "mode_name")
    private String modeName;
    @Column(name = "mode_type")
    private Integer modeType;
    @Column(name = "mode_week")
    private String modeWeek;
    @Column(name = "mode_time")
    private String modeTime;
    @Column(name = "mode_cameras")
    private String modeCameras;
    @Column(name = "mode_para")
    private String modePara;
    @Column(name = "mode_flag")
    private Integer modeFlag;

    public IaMode() {
    }

    public IaMode(Integer modeId) {
        this.modeId = modeId;
    }

    public Integer getModeId() {
        return modeId;
    }

    public void setModeId(Integer modeId) {
        this.modeId = modeId;
    }

    public String getModeName() {
        return modeName;
    }

    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    public Integer getModeType() {
        return modeType;
    }

    public void setModeType(Integer modeType) {
        this.modeType = modeType;
    }

    public String getModeWeek() {
        return modeWeek;
    }

    public void setModeWeek(String modeWeek) {
        this.modeWeek = modeWeek;
    }

    public String getModeTime() {
        return modeTime;
    }

    public void setModeTime(String modeTime) {
        this.modeTime = modeTime;
    }

    public String getModeCameras() {
        return modeCameras;
    }

    public void setModeCameras(String modeCameras) {
        this.modeCameras = modeCameras;
    }

    public String getModePara() {
        return modePara;
    }

    public void setModePara(String modePara) {
        this.modePara = modePara;
    }

    public Integer getModeFlag() {
        return modeFlag;
    }

    public void setModeFlag(Integer modeFlag) {
        this.modeFlag = modeFlag;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modeId != null ? modeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IaMode)) {
            return false;
        }
        IaMode other = (IaMode) object;
        if ((this.modeId == null && other.modeId != null) || (this.modeId != null && !this.modeId.equals(other.modeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.IaMode[ modeId=" + modeId + " ]";
    }
    
}
