/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "cameras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cameras.findAll", query = "SELECT c FROM Cameras c"),
    @NamedQuery(name = "Cameras.findByCameraId", query = "SELECT c FROM Cameras c WHERE c.cameraId = :cameraId"),
    @NamedQuery(name = "Cameras.findByCameraName", query = "SELECT c FROM Cameras c WHERE c.cameraName = :cameraName"),
    @NamedQuery(name = "Cameras.findByIp", query = "SELECT c FROM Cameras c WHERE c.ip = :ip"),
    @NamedQuery(name = "Cameras.findByPort", query = "SELECT c FROM Cameras c WHERE c.port = :port"),
    @NamedQuery(name = "Cameras.findByStreamType", query = "SELECT c FROM Cameras c WHERE c.streamType = :streamType"),
    @NamedQuery(name = "Cameras.findByStatus", query = "SELECT c FROM Cameras c WHERE c.status = :status"),
    @NamedQuery(name = "Cameras.findByChannel", query = "SELECT c FROM Cameras c WHERE c.channel = :channel"),
    @NamedQuery(name = "Cameras.findByAddress", query = "SELECT c FROM Cameras c WHERE c.address = :address"),
    @NamedQuery(name = "Cameras.findByLoginName", query = "SELECT c FROM Cameras c WHERE c.loginName = :loginName"),
    @NamedQuery(name = "Cameras.findByPassword", query = "SELECT c FROM Cameras c WHERE c.password = :password"),
    @NamedQuery(name = "Cameras.findByCompanyName", query = "SELECT c FROM Cameras c WHERE c.companyName = :companyName"),
    @NamedQuery(name = "Cameras.findByAffiliation", query = "SELECT c FROM Cameras c WHERE c.affiliation = :affiliation"),
    @NamedQuery(name = "Cameras.findByRemark", query = "SELECT c FROM Cameras c WHERE c.remark = :remark"),
    @NamedQuery(name = "Cameras.findBySceneId", query = "SELECT c FROM Cameras c WHERE c.sceneId = :sceneId"),
    @NamedQuery(name = "Cameras.findByPeopleCount", query = "SELECT c FROM Cameras c WHERE c.peopleCount = :peopleCount"),
    @NamedQuery(name = "Cameras.findByCountDatetime", query = "SELECT c FROM Cameras c WHERE c.countDatetime = :countDatetime"),
    @NamedQuery(name = "Cameras.findBySavedProcess", query = "SELECT c FROM Cameras c WHERE c.savedProcess = :savedProcess"),
    @NamedQuery(name = "Cameras.findByDeleteNumberWhenExceed", query = "SELECT c FROM Cameras c WHERE c.deleteNumberWhenExceed = :deleteNumberWhenExceed"),
    @NamedQuery(name = "Cameras.findByMaxPerserveFrameNumber", query = "SELECT c FROM Cameras c WHERE c.maxPerserveFrameNumber = :maxPerserveFrameNumber"),
    @NamedQuery(name = "Cameras.findByHistorySaveDaynum", query = "SELECT c FROM Cameras c WHERE c.historySaveDaynum = :historySaveDaynum")})
public class Cameras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "camera_id")
    private Integer cameraId;
    @Column(name = "camera_name")
    private String cameraName;
    @Column(name = "ip")
    private String ip;
    @Column(name = "port")
    private Integer port;
    @Column(name = "stream_type")
    private Integer streamType;
    @Column(name = "status")
    private Integer status;
    @Column(name = "channel")
    private Integer channel;
    @Column(name = "address")
    private String address;
    @Column(name = "login_name")
    private String loginName;
    @Column(name = "password")
    private String password;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "affiliation")
    private String affiliation;
    @Column(name = "remark")
    private String remark;
    @Column(name = "scene_id")
    private String sceneId;
    @Column(name = "people_count")
    private Integer peopleCount;
    @Column(name = "count_datetime")
    private String countDatetime;
    @Column(name = "saved_process")
    private String savedProcess;
    @Column(name = "delete_number_when_exceed")
    private String deleteNumberWhenExceed;
    @Column(name = "max_perserve_frame_number")
    private String maxPerserveFrameNumber;
    @Column(name = "history_save_daynum")
    private String historySaveDaynum;
    @OneToMany(mappedBy = "cameraId")
    private Collection<IaAlarm> iaAlarmCollection;
    @OneToMany(mappedBy = "cameraId")
    private Collection<DynamicFrames> dynamicFramesCollection;

    public Cameras() {
    }

    public Cameras(Integer cameraId) {
        this.cameraId = cameraId;
    }

    public Integer getCameraId() {
        return cameraId;
    }

    public void setCameraId(Integer cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getStreamType() {
        return streamType;
    }

    public void setStreamType(Integer streamType) {
        this.streamType = streamType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSceneId() {
        return sceneId;
    }

    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }

    public Integer getPeopleCount() {
        return peopleCount;
    }

    public void setPeopleCount(Integer peopleCount) {
        this.peopleCount = peopleCount;
    }

    public String getCountDatetime() {
        return countDatetime;
    }

    public void setCountDatetime(String countDatetime) {
        this.countDatetime = countDatetime;
    }

    public String getSavedProcess() {
        return savedProcess;
    }

    public void setSavedProcess(String savedProcess) {
        this.savedProcess = savedProcess;
    }

    public String getDeleteNumberWhenExceed() {
        return deleteNumberWhenExceed;
    }

    public void setDeleteNumberWhenExceed(String deleteNumberWhenExceed) {
        this.deleteNumberWhenExceed = deleteNumberWhenExceed;
    }

    public String getMaxPerserveFrameNumber() {
        return maxPerserveFrameNumber;
    }

    public void setMaxPerserveFrameNumber(String maxPerserveFrameNumber) {
        this.maxPerserveFrameNumber = maxPerserveFrameNumber;
    }

    public String getHistorySaveDaynum() {
        return historySaveDaynum;
    }

    public void setHistorySaveDaynum(String historySaveDaynum) {
        this.historySaveDaynum = historySaveDaynum;
    }

    @XmlTransient
    public Collection<IaAlarm> getIaAlarmCollection() {
        return iaAlarmCollection;
    }

    public void setIaAlarmCollection(Collection<IaAlarm> iaAlarmCollection) {
        this.iaAlarmCollection = iaAlarmCollection;
    }

    @XmlTransient
    public Collection<DynamicFrames> getDynamicFramesCollection() {
        return dynamicFramesCollection;
    }

    public void setDynamicFramesCollection(Collection<DynamicFrames> dynamicFramesCollection) {
        this.dynamicFramesCollection = dynamicFramesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cameraId != null ? cameraId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cameras)) {
            return false;
        }
        Cameras other = (Cameras) object;
        if ((this.cameraId == null && other.cameraId != null) || (this.cameraId != null && !this.cameraId.equals(other.cameraId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.Cameras[ cameraId=" + cameraId + " ]";
    }
    
}
