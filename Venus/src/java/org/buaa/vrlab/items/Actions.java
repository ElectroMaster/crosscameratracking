/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.items;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author exia
 */
@Entity
@Table(name = "actions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actions.findAll", query = "SELECT a FROM Actions a"),
    @NamedQuery(name = "Actions.findByActionId", query = "SELECT a FROM Actions a WHERE a.actionId = :actionId"),
    @NamedQuery(name = "Actions.findByActionName", query = "SELECT a FROM Actions a WHERE a.actionName = :actionName")})
public class Actions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "action_id")
    private Integer actionId;
    @Basic(optional = false)
    @Column(name = "action_name")
    private String actionName;
    @OneToMany(mappedBy = "actionId")
    private Collection<DynamicFrames> dynamicFramesCollection;

    public Actions() {
    }

    public Actions(Integer actionId) {
        this.actionId = actionId;
    }

    public Actions(Integer actionId, String actionName) {
        this.actionId = actionId;
        this.actionName = actionName;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    @XmlTransient
    public Collection<DynamicFrames> getDynamicFramesCollection() {
        return dynamicFramesCollection;
    }

    public void setDynamicFramesCollection(Collection<DynamicFrames> dynamicFramesCollection) {
        this.dynamicFramesCollection = dynamicFramesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actionId != null ? actionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actions)) {
            return false;
        }
        Actions other = (Actions) object;
        if ((this.actionId == null && other.actionId != null) || (this.actionId != null && !this.actionId.equals(other.actionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.buaa.vrlab.items.Actions[ actionId=" + actionId + " ]";
    }
    
}
