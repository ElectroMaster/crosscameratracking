/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.buaa.vrlab.managers;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author exia
 */
public class ItemManager {
    private static final String PUNANME = "VenusPU";
    private static EntityManagerFactory emf = null;
    private static EntityManager em;
    private static ItemManager theItemManager;
    
    private ItemManager(){
        // create factory 
        if(emf == null){
            emf = Persistence.createEntityManagerFactory(PUNANME);
            // clear cache 
            Cache theCache = emf.getCache();
            theCache.evictAll();
            
            em = emf.createEntityManager();
            em.clear();
        }
    }
    
    public static ItemManager getManager(){
        if (theItemManager == null){
            theItemManager = new ItemManager();
        }
        return theItemManager;
    }
    
    public static EntityManager getEntityManager(){
        ItemManager manager = getManager();
        return manager.em;
    }
    
    public static EntityTransaction getTransaction(){
        ItemManager manager = getManager();
        return manager.em.getTransaction();
    }
}
