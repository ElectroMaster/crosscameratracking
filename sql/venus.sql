
CREATE TABLE profil (
                profil_id INT AUTO_INCREMENT NOT NULL,
                privilage VARCHAR(30),
                PRIMARY KEY (profil_id)
);


CREATE TABLE user (
                user_id INT AUTO_INCREMENT NOT NULL,
                surname VARCHAR(30),
                name VARCHAR(30),
                uid VARCHAR(256) NOT NULL,
                password VARCHAR(256) NOT NULL,
                email VARCHAR(256),
                telephone VARCHAR(30),
                profil_id INT NOT NULL,
                user_valide BOOLEAN NOT NULL,
                PRIMARY KEY (user_id)
);

ALTER TABLE user MODIFY COLUMN surname VARCHAR(30) COMMENT '姓';


CREATE TABLE user_connection (
                connection_code VARCHAR(128) NOT NULL,
                connection_expire DATETIME NOT NULL,
                user_id INT NOT NULL,
                PRIMARY KEY (connection_code)
);


CREATE TABLE actions (
                action_id INT AUTO_INCREMENT NOT NULL,
                action_name VARCHAR(255) NOT NULL,
                PRIMARY KEY (action_id)
);


CREATE TABLE ia_mode (
                mode_id INT AUTO_INCREMENT NOT NULL,
                mode_name VARCHAR(255) NOT NULL,
                mode_type INT DEFAULT 0 NOT NULL,
                mode_week VARCHAR(255) NOT NULL,
                mode_time VARCHAR(255) NOT NULL,
                mode_cameras VARCHAR(255) NOT NULL,
                mode_para VARCHAR(255),
                mode_flag INT NOT NULL,
                PRIMARY KEY (mode_id)
);

ALTER TABLE ia_mode MODIFY COLUMN mode_id INTEGER COMMENT '预案ID';

ALTER TABLE ia_mode MODIFY COLUMN mode_name VARCHAR(255) COMMENT '预案名';

ALTER TABLE ia_mode MODIFY COLUMN mode_type INTEGER COMMENT '预案类型\n0区域人数报警\n1目标临近报警\n2目标蹲/卧倒报警';

ALTER TABLE ia_mode MODIFY COLUMN mode_week VARCHAR(255) COMMENT '按星期重复\n例：星期三,星期四,星期五,星期六';

ALTER TABLE ia_mode MODIFY COLUMN mode_time VARCHAR(255) COMMENT '重复时间\n例：0900-1200,1300-1600';

ALTER TABLE ia_mode MODIFY COLUMN mode_cameras VARCHAR(255) COMMENT ',212,215,216';

ALTER TABLE ia_mode MODIFY COLUMN mode_para VARCHAR(255) COMMENT '警：包围框高宽比阈值\n\nNULL为默认值';

ALTER TABLE ia_mode MODIFY COLUMN mode_flag INTEGER COMMENT '预案状态 1可用 0禁止';


CREATE TABLE ia_config (
                config_id INT AUTO_INCREMENT NOT NULL,
                analyse_interval VARCHAR(32) DEFAULT 1,
                pic_clean_flag VARCHAR(32) DEFAULT 0,
                hide_notrunning_flag VARCHAR(32) DEFAULT 0,
                PRIMARY KEY (config_id)
);

ALTER TABLE ia_config MODIFY COLUMN analyse_interval VARCHAR(32) COMMENT '分析间隔 秒数';

ALTER TABLE ia_config MODIFY COLUMN pic_clean_flag VARCHAR(32) COMMENT '是否清除本地图片';


CREATE TABLE cameras (
                camera_id INT AUTO_INCREMENT NOT NULL,
                camera_name VARCHAR(255) NOT NULL,
                ip VARCHAR(128),
                port INT,
                stream_type INT,
                status INT,
                channel INT,
                address VARCHAR(255),
                login_name VARCHAR(128),
                password VARCHAR(128),
                company_name VARCHAR(128),
                affiliation VARCHAR(128),
                remark VARCHAR(128),
                scene_id VARCHAR(128),
                people_count INT,
                count_datetime VARCHAR(45),
                saved_process VARCHAR(256),
                delete_number_when_exceed VARCHAR(45),
                max_perserve_frame_number VARCHAR(45),
                history_save_daynum VARCHAR(45),
                PRIMARY KEY (camera_id)
);

ALTER TABLE cameras MODIFY COLUMN camera_id INTEGER COMMENT 'UUID';

ALTER TABLE cameras MODIFY COLUMN camera_name VARCHAR(255) COMMENT '相机名称';

ALTER TABLE cameras MODIFY COLUMN ip VARCHAR(128) COMMENT '相机ip';

ALTER TABLE cameras MODIFY COLUMN port INTEGER COMMENT '端口';

ALTER TABLE cameras MODIFY COLUMN stream_type INTEGER COMMENT '0：主码流 1：辅码流  2：虚拟码流';

ALTER TABLE cameras MODIFY COLUMN status INTEGER COMMENT '码流状态 0 开启、重启 2关闭';

ALTER TABLE cameras MODIFY COLUMN channel INTEGER COMMENT '通道';

ALTER TABLE cameras MODIFY COLUMN address VARCHAR(255) COMMENT '相机地址';

ALTER TABLE cameras MODIFY COLUMN login_name VARCHAR(128) COMMENT '相机登录名';

ALTER TABLE cameras MODIFY COLUMN password VARCHAR(128) COMMENT '相机密码';

ALTER TABLE cameras MODIFY COLUMN company_name VARCHAR(128) COMMENT '公司名称';

ALTER TABLE cameras MODIFY COLUMN scene_id VARCHAR(128) COMMENT '标识为关于场景配置 场景id';

ALTER TABLE cameras MODIFY COLUMN people_count INTEGER COMMENT '统计结果人数';

ALTER TABLE cameras MODIFY COLUMN count_datetime VARCHAR(45) COMMENT '统计结果得到的时间，为方便存储其他格式时间设置种类为varchar';

ALTER TABLE cameras MODIFY COLUMN saved_process VARCHAR(256) COMMENT '当前处理到哪张图';

ALTER TABLE cameras MODIFY COLUMN delete_number_when_exceed VARCHAR(45) COMMENT '待分析图片超过个数时删除';

ALTER TABLE cameras MODIFY COLUMN max_perserve_frame_number VARCHAR(45) COMMENT '待分析图片保留个数';

ALTER TABLE cameras MODIFY COLUMN history_save_daynum VARCHAR(45) COMMENT '历史保留天数';


CREATE TABLE dynamic_frames (
                frame_id INT AUTO_INCREMENT NOT NULL,
                camera_id INT,
                frame_name VARCHAR(255),
                time_cap VARCHAR(255),
                frame_path VARCHAR(2048),
                people_count INT,
                bboxs VARCHAR(2048),
                action_cls_flag BOOLEAN DEFAULT false,
                action_id INT,
                PRIMARY KEY (frame_id)
);

ALTER TABLE dynamic_frames COMMENT 'frames in which there are pedestrains';

ALTER TABLE dynamic_frames MODIFY COLUMN camera_id INTEGER COMMENT 'UUID';

ALTER TABLE dynamic_frames MODIFY COLUMN frame_name VARCHAR(255) COMMENT '图片帧存储名称';

ALTER TABLE dynamic_frames MODIFY COLUMN time_cap VARCHAR(255) COMMENT '图片帧被捕获时间，由于精确到毫秒故不能用timestamp存储';

ALTER TABLE dynamic_frames MODIFY COLUMN frame_path VARCHAR(2048) COMMENT '图片帧存储路径fullpath';

ALTER TABLE dynamic_frames MODIFY COLUMN people_count INTEGER COMMENT '图片帧中人的个数';

ALTER TABLE dynamic_frames MODIFY COLUMN bboxs VARCHAR(2048) COMMENT ',height2, width2 ], ...';

ALTER TABLE dynamic_frames MODIFY COLUMN action_cls_flag BOOLEAN COMMENT '该图片帧是否进行过动作识别';


CREATE TABLE ia_alarm (
                alarm_id INT AUTO_INCREMENT NOT NULL,
                camera_id INT,
                alarm_type VARCHAR(30),
                server_id VARCHAR(255),
                alarm_state INT DEFAULT 1,
                photo_address VARCHAR(255),
                nvr_time VARCHAR(255),
                add_time VARCHAR(255),
                alarm_vpt VARCHAR(2048),
                remark VARCHAR(500) DEFAULT 1,
                PRIMARY KEY (alarm_id)
);

ALTER TABLE ia_alarm MODIFY COLUMN alarm_id INTEGER COMMENT '主键id';

ALTER TABLE ia_alarm MODIFY COLUMN camera_id INTEGER COMMENT 'UUID';

ALTER TABLE ia_alarm MODIFY COLUMN alarm_type VARCHAR(30) COMMENT '报警类型 多个时逗号分隔\n0区域人数报警\n1目标临近报警\n2目标蹲/卧倒报警';

ALTER TABLE ia_alarm MODIFY COLUMN server_id VARCHAR(255) COMMENT '服务器id';

ALTER TABLE ia_alarm MODIFY COLUMN photo_address VARCHAR(255) COMMENT '对应图片地址';

ALTER TABLE ia_alarm MODIFY COLUMN nvr_time VARCHAR(255) COMMENT 'nvr时间';

ALTER TABLE ia_alarm MODIFY COLUMN add_time VARCHAR(255) COMMENT '添加时间 为本地时间';

ALTER TABLE ia_alarm MODIFY COLUMN alarm_vpt VARCHAR(2048) COMMENT '区域坐标';

ALTER TABLE ia_alarm MODIFY COLUMN remark VARCHAR(500) COMMENT '0 为用户不可见，逻辑删除 1为用户可见';


ALTER TABLE user ADD CONSTRAINT profil_user_fk
FOREIGN KEY (profil_id)
REFERENCES profil (profil_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE user_connection ADD CONSTRAINT user_connection_fk
FOREIGN KEY (user_id)
REFERENCES user (user_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE dynamic_frames ADD CONSTRAINT ations_dynamic_frame_fk
FOREIGN KEY (action_id)
REFERENCES actions (action_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ia_alarm ADD CONSTRAINT cameras_ia_alarm_fk
FOREIGN KEY (camera_id)
REFERENCES cameras (camera_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE dynamic_frames ADD CONSTRAINT cameras_dynamic_frame_fk
FOREIGN KEY (camera_id)
REFERENCES cameras (camera_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
