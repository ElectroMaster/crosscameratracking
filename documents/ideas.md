# Ideas
>Author: CAI Pengfei
### Client:
- 使用运动检测获得含有运动物体的图片，

### Server
**接收端工作**：
- 服务端对图片进行接收，使用faster rcnn进行识别，进行筛选和储存操作
- 筛选：
  - 识别结果包含人，
  - 属于连续片段：查看动作数据集的动作平均时间t，平均帧数为k，那么连续帧间隔时长则应为t/k，进而用于判断是否为动作片段。--第二张图片减去第一张图片的时间若大于t/k则为非连续片段
- 储存： 将相机ip，相机名称，图片路径，图片捕获时间，连续片段编号写入数据库。

**动作识别部分**：
- 使用c3d（后期做改进）对连续片段进行动作识别，并将分类结果写入数据库。

**web**
- 用于对数据库进行管理,界面有登陆界面，相机管理界面，以及结果查看界面。
