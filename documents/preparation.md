## 1. Configurations for mysql database.
> OS: ubuntu

1. Install mysql
```
sudo apt install mysql-server
```
To start mysql, normally you just need to tap `mysql -u root -p` and enter password, then you can use mysql. If you haven't set password for root, just press `enter`. **But** there are always errors wait for us. :(.

If you encountered pb like this:
```
ERROR 1698 (28000): Access denied for user 'root'@'localhost'
```
These steps will help you. In terminal:
```
su root
mysql
mysql> USE mysql;
mysql> SELECT User, Host, plugin FROM mysql.user;
```
and you can see a table
```
+------------------+-----------+-----------------------+
| User             | Host      | plugin                |
+------------------+-----------+-----------------------+
| root             | localhost | auth_socket           |
| mysql.session    | localhost | mysql_native_password |
| mysql.sys        | localhost | mysql_native_password |
| debian-sys-maint | localhost | mysql_native_password |
+------------------+-----------+-----------------------+

```
you need to change plugin of user `root`.
```
mysql> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
mysql> FLUSH PRIVILEGES;
mysql> exit;
service mysql restart
```
now try again `mysql -u root -p` will solve the pb.

2. create user for mysql
```
mysql> GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost' IDENTIFIED BY 'password';
mysql> exit;
```
then you can use `mysql username -p` to access mysql. (maybe not need password).

**PS**: to use `su root` may be `sudo passwd` can help you create a new unix password for root.

3. install GUI tools for mysql
```
sudo apt install mysql-workbench
```

## 2. Installation of SQL power architecture
download page: http://www.bestofbi.com/page/architect_download_os


## 3. Darknet yolov3 cofiguration.

please refer to https://github.com/AlexeyAB/darknet.

1. Install cuda 10.0

download ![cuda](https://developer.nvidia.com/cuda-10.0-download-archive) from nvidia site.

2. Download cudnn v7 for cuda 10.0

download ![cudnn](https://developer.nvidia.com/rdp/cudnn-download) from nvidia site

3. Install opencv 3.x
```
sudo apt install libopencv-dev
```


## 4. java spring connect mysql
when you installed mysql jdbc connector, the jar file locate at path `/usr/share/java/mysql-connector-java-8.0.16.jar`


if you encountered pb with time error when you connect with mysql using java, plz check your url:
```
jdbc:mysql://localhost:3306/venus?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC
```
