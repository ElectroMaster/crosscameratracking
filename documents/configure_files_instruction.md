## Client

- server_ip
- server_port
- camera_id: by default `0`
- vibe_init_frames_num: because Vibe algorithm need to initialisation, it's in fact the number of frames that we ignore at first. 初始化vibe需要的帧数
- image_postfix: by default `.jpg`. this will influence the image encode and decode manner.
- images_storage_location: by default, `images`. **attention**, images file storage location full path will be `images/<client_ip>/2019-04-16_12:04:43_330.jpg`.
- show_frames: by default `true`. used for showing images captured by camera.
- show_masks: by default `true`. used for showing image processed by vibe.


## Server
- server_ip: by default `127.0.0.1`
- server_port
- show_frames: by default `true`. used for showing images received.
