#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <string.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <chrono>

#define OPENCV
#define GPU
#include "yolo_v2_class.hpp"
#include "json.hpp"
#include "vibe.h"
#include "time.h"
#include "utils.h"
#include "ini.h"

#define BUFFER_SIZE 1024

using namespace std;
using namespace cv;
using json = nlohmann::json;

typedef struct
{
    const char* server_ip;
    int server_port;
    int device_index;
    int camera_id;
    const char* camera_name;
    int vibe_init_frames_num;
    const char* image_postfix;
    const char* images_storage_location;
    bool show_frames;
    bool show_masks;
    const char* show_frames_window_name;
    const char* show_masks_window_name;
} configuration;

static int config_handler(void* user, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("client", "server_ip")){
        pconfig->server_ip = strdup(value);
    } else if(MATCH("client", "server_port")){
        pconfig->server_port = atoi(value);
    } else if(MATCH("client", "device_index")){
        pconfig->device_index = atoi(value);
    } else if(MATCH("client", "camera_name")){
        pconfig->camera_name = strdup(value);
    } else if(MATCH("client", "camera_id")){
        pconfig->camera_id = atoi(value);
    } else if(MATCH("client", "vibe_init_frames_num")){
        pconfig->vibe_init_frames_num = atoi(value);
    } else if(MATCH("client", "image_postfix")){
        pconfig->image_postfix = strdup(value);
    } else if(MATCH("client", "images_storage_location")){
        pconfig->images_storage_location = strdup(value);
    }
    else if(MATCH("client", "show_frames")){
        if((strcmp(strdup(value), "true")==0)||(atoi(value) == 1)){
            pconfig->show_frames = true;
        } else {
            pconfig->show_frames = false;
        }
    } else if(MATCH("client", "show_masks")){
        if((strcmp(strdup(value), "true")==0)||(atoi(value) == 1)){
            pconfig->show_masks = true;
        } else {
            pconfig->show_masks = false;
        }
    } else if(MATCH("client", "show_frames_window_name")){
        pconfig->show_frames_window_name = strdup(value);
    } else if(MATCH("client", "show_masks_window_name")){
        pconfig->show_masks_window_name = strdup(value);
    }
    else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}


int main()
{
    configuration config;
    if (ini_parse("config-cli.ini", config_handler, &config) < 0) {
        printf("[ERROR] Can't load configuration file.\n");
        return -1;
    }

    string names_file = "cfg/obj.names";
    string cfg_file = "cfg/yolo-obj-test.cfg";
    string weights_file = "cfg/yolo-obj-train_4000.weights";
    Detector detector(cfg_file, weights_file);
    auto obj_names = objects_names_from_file(names_file);



    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(config.server_ip);
    server_addr.sin_port = htons(config.server_port);

    VideoCapture cap(config.device_index);//0->rtsp
//    VideoCapture cap("test.mp4");

    if(!cap.isOpened()) {
        cout<< "Could not open the camera" <<  endl;
        return -1;
    }

    Mat frame, gray, mask;
    ViBe_BGS Vibe_Bgs;

    int cnt=0;
    clock_t start, finish;
    double time_gap;

    int server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(connect(server_sock, (struct sockaddr*)&server_addr, sizeof(server_addr))<0){
        perror("CONNECT: ");
        exit(-1);
    }
    // this two phrase are very important, this ensure if server haven't received msg head, we can send msg again.
    struct timeval timeout = {1,0};
    setsockopt(server_sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(struct timeval));

    while(true) {   //begin loop
        auto frame_start = chrono::steady_clock::now();

        start = clock();
        cap >> frame;
        if(frame.empty()) {
            cerr<<"[client] VideoCapture(0) error!"<<endl;
        }

        cout<< ++cnt << ":"<< frame.isContinuous()<<"," <<frame.size()<<endl;
        cvtColor(frame, gray, CV_RGB2GRAY);
        if (cnt == 1)   // Initialisation of background
        {
            Vibe_Bgs.init(gray);
            Vibe_Bgs.processFirstFrame(gray);
            cout<<" Training GMM complete!"<<endl;
        }
        else
        {
            Vibe_Bgs.testAndUpdate(gray);
            if(Vibe_Bgs.DetectMVObj()&&cnt>config.vibe_init_frames_num)   //cnt>200
            {
                auto chrono_start = chrono::steady_clock::now();
                std::vector<bbox_t> result_vec = detector.detect(frame);
                auto chrono_end = chrono::steady_clock::now();
                chrono::duration<double> spent = chrono_end - chrono_start;
                std::cout << " Yolo detect time: " << spent.count()*1000 << " ms \n";
                if(person_detect_result(result_vec, obj_names)){
                    start = clock();
                    vector<uchar> vec;
                    imencode(config.image_postfix, frame, vec);
                    int image_data_len = vec.size();
                    string current_time = getTime();
                    string filename = current_time + config.image_postfix;

                    json msg_json;
                    msg_json["filename"] = filename.c_str();
                    msg_json["image_data_length"] = image_data_len;
                    msg_json["images_directory"] = config.images_storage_location;
                    msg_json["date"] = current_time.c_str();
                    msg_json["camera_name"] = config.camera_name;
                    msg_json["camera_id"] = config.camera_id;
                    msg_json["people_count"] = get_person_num(result_vec, obj_names);
                    msg_json["bboxs"] = get_bbox_str(result_vec, obj_names).c_str();
                    char msg[BUFFER_SIZE];
                    bzero(msg, BUFFER_SIZE);
                    strcpy(msg, msg_json.dump().c_str());

                    cout<<"[INFO] Send message head ..."<<endl;
                    cout<<"message: "<<msg<<endl;
                    int len = send(server_sock, msg, BUFFER_SIZE, 0);
                    cout<<"len: "<<len<<endl;
                    if(len<=0){
                        cout<<"[ERROR] Lose connection with server."<<endl;
                        break;
                    }
                    char msg_valide[BUFFER_SIZE];
                    bzero(msg_valide, BUFFER_SIZE);
                    len = recv(server_sock, msg_valide, BUFFER_SIZE, 0);
                    if(strcmp(msg_valide, "OK") == 0){
                        cout<<"[ OK ] Server received message head successfully"<<endl;
                    }else{
                        cout<<"[ERROR] Server cannot receive message head"<<endl;
                        // if server cannot receive message head, we don't need to send image data any more.
                        continue;
                    }


                    int pos = 0;
                    cout<<"[INFO] Send image data ..."<<endl;
                    while(pos<image_data_len){
                        uchar temps_data[BUFFER_SIZE];
                        memset(temps_data, 0, sizeof(temps_data));
                        for(int i=pos; i<pos+BUFFER_SIZE;i++){
                            if (i>=(image_data_len))
                                break;
                            temps_data[i-pos] = vec[i];
                        }
                        send(server_sock, temps_data, BUFFER_SIZE, 0);
                        pos = pos+BUFFER_SIZE;
                    }
                    bzero(msg_valide, BUFFER_SIZE);
                    len = recv(server_sock, msg_valide, BUFFER_SIZE, 0);
                    if(strcmp(msg_valide, "OK") == 0){
                        cout<<"[ OK ] Server received image data successfully"<<endl;
                    }else{
                        cout<<"[ERROR] Server cannot receive image data head"<<endl;
                        // if server cannot receive message head, we don't need to send image data any more.
                        continue;
                    }
                    cout<<"[ OK ] Send image data over. Image data length: "<<image_data_len<<endl;
                    finish = clock();
                    time_gap = (double)(finish-start)/CLOCKS_PER_SEC;
                    cout<<"[INFO] time to send: "<<time_gap*1000<<" ms"<<endl;
                    cout<<"-------------"<<endl;
                    draw_boxes(frame, result_vec, obj_names);
                }

            }
            if(config.show_masks){
                mask = Vibe_Bgs.getMask();
                morphologyEx(mask, mask, MORPH_OPEN, Mat());
                imshow(config.show_masks_window_name, mask);
            }
        }
        if(config.show_frames){
            imshow(config.show_frames_window_name, frame);
        }
        if(config.show_frames || config.show_masks){
            if(cv::waitKey(1) == 'q') {
                break;
            }
        }
        auto frame_end = chrono::steady_clock::now();
        chrono::duration<double> spent = frame_end - frame_start;
        std::cout << " Process one image time: " << spent.count()*1000 << " ms \n";
    }
    close(server_sock);
    return 0;
}

