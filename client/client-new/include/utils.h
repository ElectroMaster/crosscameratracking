#ifndef UTILS_H
#define UTILS_H



#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>

#include <iostream>
//#include <string>

#include "yolo_v2_class.hpp"

std::vector<std::string> objects_names_from_file(std::string const filename) {
    std::ifstream file(filename);
    std::vector<std::string> file_lines;
    if (!file.is_open()) return file_lines;
    for(std::string line; getline(file, line);) file_lines.push_back(line);
    std::cout << "object names loaded \n";
    return file_lines;
}

bool person_detect_result(std::vector<bbox_t> const result_vec, std::vector<std::string> const obj_names) {
    bool person_flag = false;
    for (auto &i : result_vec) {
        if (obj_names.size() > i.obj_id){

            if((strcmp(obj_names[i.obj_id].c_str(), "person") == 0) || (strcmp(obj_names[i.obj_id].c_str(), "Person") == 0)){
                person_flag = true;
                std::cout << obj_names[i.obj_id] << " - ";
                std::cout << "obj_id = " << i.obj_id << ",  x = " << i.x << ", y = " << i.y
                    << ", w = " << i.w << ", h = " << i.h
                    << std::setprecision(3) << ", prob = " << i.prob << std::endl;
            }
        }

    }
    return person_flag;
}


int get_person_num(std::vector<bbox_t> const result_vec, std::vector<std::string> const obj_names){
    int nb_person = 0;
    for (auto &i : result_vec) {
        if (obj_names.size() > i.obj_id){

            if((strcmp(obj_names[i.obj_id].c_str(), "person") == 0) || (strcmp(obj_names[i.obj_id].c_str(), "Person") == 0)){
                nb_person += 1;
            }
        }

    }
    return nb_person;
}

string get_bbox_str(std::vector<bbox_t> const result_vec, std::vector<std::string> const obj_names) {
    char bbox_array[100];
    string res = "";
    for (auto &i : result_vec) {
        if (obj_names.size() > i.obj_id){

            if((strcmp(obj_names[i.obj_id].c_str(), "person") == 0) || (strcmp(obj_names[i.obj_id].c_str(), "Person") == 0)){
                sprintf(bbox_array, "[%d, %d, %d, %d]", i.x, i.y, i.w, i.h);
                string sub_str = bbox_array;
//                std::cout<<sub_str<<std::endl;
                res = res + sub_str +", ";
            }
        }

    }
    res = res.substr(0, res.size()-2);
    return res;
}




void draw_boxes(cv::Mat mat_img, std::vector<bbox_t> result_vec, std::vector<std::string> obj_names,
    int current_det_fps = -1, int current_cap_fps = -1)
{
    int const colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };

    for (auto &i : result_vec) {

        if (obj_names.size() > i.obj_id) {
            std::string obj_name = obj_names[i.obj_id];
            if((strcmp(obj_names[i.obj_id].c_str(), "person") != 0) && (strcmp(obj_names[i.obj_id].c_str(), "Person") != 0)){
                continue;
            }
            cv::Scalar color = obj_id_to_color(i.obj_id);
            cv::rectangle(mat_img, cv::Rect(i.x, i.y, i.w, i.h), color, 2);
            if (i.track_id > 0) obj_name += " - " + std::to_string(i.track_id);
            cv::Size const text_size = getTextSize(obj_name, cv::FONT_HERSHEY_COMPLEX_SMALL, 1.2, 2, 0);
            int const max_width = (text_size.width > i.w + 2) ? text_size.width : (i.w + 2);
            cv::rectangle(mat_img, cv::Point2f(std::max((int)i.x - 1, 0), std::max((int)i.y - 30, 0)),
                cv::Point2f(std::min((int)i.x + max_width, mat_img.cols-1), std::min((int)i.y, mat_img.rows-1)),
                color, CV_FILLED, 8, 0);
            putText(mat_img, obj_name, cv::Point2f(i.x, i.y - 10), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.2, cv::Scalar(0, 0, 0), 2);
        }
    }
    if (current_det_fps >= 0 && current_cap_fps >= 0) {
        std::string fps_str = "FPS detection: " + std::to_string(current_det_fps) + "   FPS capture: " + std::to_string(current_cap_fps);
        putText(mat_img, fps_str, cv::Point2f(10, 20), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.2, cv::Scalar(50, 255, 0), 2);
    }
}



string getTime();

#endif // UTILS_H

