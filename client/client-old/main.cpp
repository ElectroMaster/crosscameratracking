// client

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include "vibe.h"
#include <iostream>
#include<time.h>
#include<utils.h>
using namespace std;
using namespace cv;

int main()
{
    int sock;
    struct sockaddr_in addr;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0){
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(5000);

    if( inet_pton(AF_INET, "219.224.167.170", &addr.sin_addr) <= 0){
        printf("inet_pton error for %s\n","SERVERIP");
        exit(0);
    }

    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0){
        perror("connect");
        exit(2);
    }

    int bbytee;
    cout << "before open the cam" << endl;

    VideoCapture cap(0);//0->rtsp

    if(!cap.isOpened()) {
        cout<< "Could not open the camera" <<  endl;
        close(sock);
        return -1;
    }

    Mat frame, gray, mask;
    frame = Mat::zeros(480, 640, CV_8UC3);
    int imgSize = frame.cols*frame.rows*3;
    ViBe_BGS Vibe_Bgs;

    int cnt=0;
    //Mat frame;
    clock_t start, finish;
    double time_gap;
    while(true) {//开始循环
        start = clock();
        cap >> frame;
        if(frame.empty()) {
            cerr<<"[client] VideoCapture(0) error!"<<endl;
        }

        cout<< ++cnt << ":"<< frame.isContinuous()<<"," <<frame.size()<<endl;
        cvtColor(frame, gray, CV_RGB2GRAY);
        if (cnt == 1)//初始化
        {
            Vibe_Bgs.init(gray);
            Vibe_Bgs.processFirstFrame(gray);
            cout<<" Training GMM complete!"<<endl;
        }
        else
        {
            Vibe_Bgs.testAndUpdate(gray);
            if(Vibe_Bgs.DetectMVObj()&&cnt>200)
            {
                if( (bbytee = send(sock, frame.data, imgSize, 0)) < 0 ) {
                    cerr<< "bytes = " << bbytee << endl;
                    break;
                }
                finish = clock();
                time_gap = (double)(finish-start)/CLOCKS_PER_SEC;
                string current_time = getTime();
                int mybbytee;
                mybbytee = send(sock, current_time.c_str(), current_time.length(), 0);

                cout<<"time to send: "<<time_gap<<endl;
            }
            mask = Vibe_Bgs.getMask();

            morphologyEx(mask, mask, MORPH_OPEN, Mat());
            imshow("mask", mask);
        }

        imshow("frame", frame);
        if(cv::waitKey(100) == 'q') {
            char *mymsg = "Beej was here!";
            bbytee = send(sock, mymsg, strlen(mymsg), 0);
            break;
        }
    }
    close(sock);
    return 0;
}
